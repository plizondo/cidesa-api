package com.hayturno.service.endpoints.impl;

import com.hayturno.data.service.ProvinciaService;
import com.hayturno.entities.Especialidad;
import com.hayturno.entities.Localidad;
import com.hayturno.entities.Provincia;
import com.hayturno.service.endpoints.ProvinciaEndpoint;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by tomas on 08/03/16.
 */
public class ProvinciaEndpointImpl implements ProvinciaEndpoint {

    @Autowired
    ProvinciaService service;



    @Override
    public List<Provincia> findAll() {
        return service.findAll();
    }

    @Override
    public List<Localidad> localidadesPorProvincia(Integer idProvincia) {
        List<Localidad> list = service.getLocalidadesByProvincia(idProvincia);
        return list;
        //  return Response.ok().entity(list).build();
    }
}

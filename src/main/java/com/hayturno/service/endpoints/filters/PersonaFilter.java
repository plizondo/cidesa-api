package com.hayturno.service.endpoints.filters;

import javax.ws.rs.QueryParam;

/**
 * Created by tomas on 09/03/16.
 */
public class PersonaFilter {

    @QueryParam("dni")
    private Integer dni;

    @QueryParam("apellido")
    private String apellido;

    @QueryParam("fmin")
    private Integer fechaMin = 0;

    @QueryParam("fmax")
    private Integer fechaMax = 999;

    @QueryParam("osid")
    private Integer obraSocialId;

    @QueryParam("localidad")
    private String localidad;


    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getFechaMin() {
        return fechaMin;
    }

    public void setFechaMin(Integer fechaMin) {
        this.fechaMin = fechaMin;
    }

    public Integer getFechaMax() {
        return fechaMax;
    }

    public void setFechaMax(Integer fechaMax) {
        this.fechaMax = fechaMax;
    }

    public Integer getObraSocialId() {
        return obraSocialId;
    }

    public void setObraSocialId(Integer obraSocialId) {
        this.obraSocialId = obraSocialId;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
}

package com.hayturno.data.service;

import com.hayturno.entities.Localidad;
import com.hayturno.entities.Provincia;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tomas on 08/03/16.
 */
public interface ProvinciaService {

    List<Provincia> findAll();

    void delete(int id);

    @Transactional(propagation = Propagation.REQUIRED)
    Provincia addProvincia(Provincia provincia);


    List<Localidad> getLocalidadesByProvincia(Integer idProvincia);
}

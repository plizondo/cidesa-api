package com.hayturno.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Created by tomas on 09/03/16.
 */
@Entity
@Table(name = "obrasocial")
@XmlRootElement(name = "obrasSociales")
public class ObraSocial {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idobrasocial")
    private Integer id;
    @Column(name = "obrasocial")
    private String obraSocial;
    @Column(name = "con_convenio")
    private Boolean conConvenio;
    @Column(name = "observaciones")
    private String observaciones;

    @ManyToMany(mappedBy = "obrasSociales")
    private Collection<Persona> personas;


    /****modifiers****/

    public Integer getId() { return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObraSocial() {
        return obraSocial;
    }

    public void setObraSocial(String obraSocial) {
        this.obraSocial = obraSocial;
    }

    public Boolean getConConvenio() {
        return conConvenio;
    }

    public void setConConvenio(Boolean conConvenio) {
        this.conConvenio = conConvenio;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Collection<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(Collection<Persona> personas) {
        this.personas = personas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ObraSocial that = (ObraSocial) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

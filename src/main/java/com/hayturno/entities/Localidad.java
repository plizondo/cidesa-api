package com.hayturno.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tomas.lingotti on 3/8/2016.
 */
@Entity
@Table(name = "localidad")
@XmlRootElement(name = "localidades")
public class Localidad {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="idlocalidad")
    private Integer id;

    @Column(name = "localidad")
    private String localidad;

    @Column(name = "idprovincia")
    private Integer idProvincia;

    /****modifiers****/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public Integer getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Integer idProvincia) {
        this.idProvincia = idProvincia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Localidad localidad1 = (Localidad) o;

        if (!id.equals(localidad1.id)) return false;
        if (localidad != null ? !localidad.equals(localidad1.localidad) : localidad1.localidad != null) return false;
        return idProvincia != null ? idProvincia.equals(localidad1.idProvincia) : localidad1.idProvincia == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (localidad != null ? localidad.hashCode() : 0);
        result = 31 * result + (idProvincia != null ? idProvincia.hashCode() : 0);
        return result;
    }
}

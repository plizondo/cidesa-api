package com.hayturno.data.service.impl;

import com.hayturno.data.repository.PersonaRepository;
import com.hayturno.data.service.PersonaService;
import com.hayturno.entities.Control;
import com.hayturno.entities.Persona;
import com.hayturno.service.endpoints.filters.PersonaFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by tomas on 09/03/16.
 */
@Service
public class PersonaServiceImpl implements PersonaService{

    @Autowired
    PersonaRepository repository;

    @PersistenceContext
    EntityManager em;

    @Override
    public Persona addPersona(Persona p) {
        return repository.save(p);
    }

    @Override
    public List<Persona> serachPersona(Integer osid) {
      /*  Query q = em.createNativeQuery(
                "select p.* from persona p " +
                "inner join obrasocial_persona op on p.idpersona = op.idpersona " +
                "inner join obrasocial os on os.idobrasocial = op.idobrasocial " +
                "where os.idobrasocial =?1 ; " );*/


        Query q = em.createQuery("SELECT p from Persona p JOIN p.obrasSociales o where o.id = :idos");

        q.setParameter("idos",osid);

        List<Persona> personaList = q.getResultList();

        return personaList;
    }

    @Override
    public void deletePersona(Integer id) {
        repository.delete(id);
        em.flush();
    }

    @Override
    public Persona updatePersona(Persona p) {
        return em.merge(p);
    }

    @Override
    public List<Control> controles(Integer idPersona) {

        Query query = em.createQuery("select c from Control c where c.idPersona = :idpersona");
        query.setParameter("idpersona",idPersona);
        List<Control> controlList = query.getResultList();
        return controlList;
    }
}

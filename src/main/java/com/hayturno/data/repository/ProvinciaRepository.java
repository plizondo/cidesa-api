package com.hayturno.data.repository;

import com.hayturno.entities.Provincia;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas on 08/03/16.
 */
public interface ProvinciaRepository extends JpaRepository<Provincia,Integer> {
}

package com.hayturno.entities;

import org.joda.time.DateTime;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tomas.lingotti on 3/9/2016.
 */
@Entity
@Table(name="control")
@XmlRootElement(name = "controles")
public class Control {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idcontrol", nullable = false)
    private Integer id;

    @Column(name = "fecha")
    private DateTime fecha;
    @Column(name = "peso")
    private Float peso;
    @Column(name = "imc")
    private Float imc;
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "talla")
    private String talla;
    @Column(name = "idpersona")
    private Integer idPersona;


    /****modifiers****/

    public DateTime getFecha() {
        return fecha;
    }

    public void setFecha(DateTime fecha) {
        this.fecha = fecha;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public Float getImc() {
        return imc;
    }

    public void setImc(Float imc) {
        this.imc = imc;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Control control = (Control) o;

        if (fecha != null ? !fecha.equals(control.fecha) : control.fecha != null) return false;
        if (peso != null ? !peso.equals(control.peso) : control.peso != null) return false;
        if (imc != null ? !imc.equals(control.imc) : control.imc != null) return false;
        if (observacion != null ? !observacion.equals(control.observacion) : control.observacion != null) return false;
        if (talla != null ? !talla.equals(control.talla) : control.talla != null) return false;
        return idPersona != null ? idPersona.equals(control.idPersona) : control.idPersona == null;

    }

    @Override
    public int hashCode() {
        int result = fecha != null ? fecha.hashCode() : 0;
        result = 31 * result + (peso != null ? peso.hashCode() : 0);
        result = 31 * result + (imc != null ? imc.hashCode() : 0);
        result = 31 * result + (observacion != null ? observacion.hashCode() : 0);
        result = 31 * result + (talla != null ? talla.hashCode() : 0);
        result = 31 * result + (idPersona != null ? idPersona.hashCode() : 0);
        return result;
    }
}

package com.hayturno.entities;

import org.joda.time.DateTime;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Created by tomas.lingotti on 3/9/2016.
 */
@Entity
@Table(name="persona")
@XmlRootElement(name = "personas")
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idpersona")
    private Integer id;

    @Column(name = "dni")
    private Integer dni;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "fecha_nacimiento")
    private DateTime fechaNacimiento;

    @Column(name = "barrio")
    private String barrio;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "celular")
    private String celular;

    @Column(name = "email")
    private String email;

    @Column(name = "consultas_disponibles")
    private Integer consultasDisponibles;

    @Column(name = "peso_inicial")
    private Float pesoInicial;

    @Column(name = "peso_saludable")
    private Float pesoSaludable;

    @Column(name = "peso_porcentaje_objetivo")
    private Integer pesoPorcentajeObjetivo;

    @Column(name = "observaciones")
    private String observeaciones;

    @Column(name = "fecha_vencimiento_plan")
    private DateTime fechaVencimientoPlan;

    @Column(name = "idlocalidad")
    private Integer idLocalidad;

    @Column(name = "idprovincia")
    private Integer idProvincia;

    @Column(name = "idestado")
    private Integer idEstado;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "obrasocial_persona")
    private Collection<ObraSocial> obrasSociales;


    /****modifiers****/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public DateTime getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(DateTime fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getConsultasDisponibles() {
        return consultasDisponibles;
    }

    public void setConsultasDisponibles(Integer consultasDisponibles) {
        this.consultasDisponibles = consultasDisponibles;
    }

    public Float getPesoInicial() {
        return pesoInicial;
    }

    public void setPesoInicial(Float pesoInicial) {
        this.pesoInicial = pesoInicial;
    }

    public Float getPesoSaludable() {
        return pesoSaludable;
    }

    public void setPesoSaludable(Float pesoSaludable) {
        this.pesoSaludable = pesoSaludable;
    }

    public Integer getPesoPorcentajeObjetivo() {
        return pesoPorcentajeObjetivo;
    }

    public void setPesoPorcentajeObjetivo(Integer pesoPorcentajeObjetivo) {
        this.pesoPorcentajeObjetivo = pesoPorcentajeObjetivo;
    }

    public String getObserveaciones() {
        return observeaciones;
    }

    public void setObserveaciones(String observeaciones) {
        this.observeaciones = observeaciones;
    }

    public DateTime getFechaVencimientoPlan() {
        return fechaVencimientoPlan;
    }

    public void setFechaVencimientoPlan(DateTime fechaVencimientoPlan) {
        this.fechaVencimientoPlan = fechaVencimientoPlan;
    }

    public Integer getIdLocalidad() {
        return idLocalidad;
    }

    public void setIdLocalidad(Integer idLocalidad) {
        this.idLocalidad = idLocalidad;
    }

    public Integer getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Integer idProvincia) {
        this.idProvincia = idProvincia;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public Collection<ObraSocial> getObrasSociales() {
        return obrasSociales;
    }

    public void setObrasSociales(Collection<ObraSocial> obrasSociales) {
        this.obrasSociales = obrasSociales;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Persona persona = (Persona) o;

        if (!id.equals(persona.id)) return false;
        return dni.equals(persona.dni);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + dni.hashCode();
        return result;
    }
}

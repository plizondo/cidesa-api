package com.hayturno.service.endpoints;

import com.hayturno.entities.Persona;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by tomas on 09/03/16.
 */
public interface PersonaEndpoint {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/b")
    Response searchPersona(@QueryParam("osid") Integer osid);

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/d/{id}")
    void deletePersona(@PathParam("id") Integer id);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/addPersona")
    Response addPersona(Persona persona);
}

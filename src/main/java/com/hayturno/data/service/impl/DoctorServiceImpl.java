package com.hayturno.data.service.impl;

import com.hayturno.data.repository.DoctorRepository;
import com.hayturno.data.repository.HorariosDocRepository;
import com.hayturno.data.repository.TurnoRepository;
import com.hayturno.data.service.DoctorService;
import com.hayturno.entities.Doctor;
import com.hayturno.entities.Estado;
import com.hayturno.entities.HorariosDoctor;
import com.hayturno.entities.Turno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by tomas on 13/03/16.
 */
@Service
@EnableTransactionManagement
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    DoctorRepository repository;

    @Autowired
    TurnoRepository turnoRepository;

    @Autowired
    HorariosDocRepository horariosDocRepository;

    @PersistenceContext
    EntityManager em;

    @Override
    public Doctor addDoctor(Doctor d) {
        return repository.save(d);
    }

    @Override
    public void deleteDoctor(Integer id) {
        repository.delete(id);
        em.flush();


    }

    @Override
    public List<Doctor> findDoctorByApellido(String apellidoDoctor) {
        Query query = em.createQuery("select d from Doctor d JOIN Persona p where  p.apellido = :apellido");
        query.setParameter("apellido",apellidoDoctor);
        List<Doctor> doctorList  = query.getResultList();
        return doctorList;
    }

    @Override
    public List<Doctor> findAll() {
        return repository.findAll();
    }


    @Override
    public Doctor updateDoctor(Doctor doctor) {
        return em.merge(doctor);
    }

    @Override
    public Turno addTurno(Turno t) {
        return turnoRepository.save(t);
    }

    @Override
    public void deleteTurno(Integer idTurno) {
        turnoRepository.delete(idTurno);
    }

    @Override
    public List<Turno> findTurnoByFecha(Date date) {
        Query query = em.createQuery("select t from Turno t where t.fecha = :fecha order by  t.fecha");
        query.setParameter("fecha",date);
        List<Turno> turnos = query.getResultList();
        return turnos;
    }

    @Override
    public Turno updateTurno(Turno turno) {
        return em.merge(turno);
    }


    @Override
    public Estado updateEstado(Integer idTurno) {

        Query query = em.createQuery("select e from Turno t JOIN Estado e where t.id = :idTurno");

        if (query.getResultList().isEmpty() ) throw  new NullPointerException("No results founded");

        Estado estado =(Estado) query.getResultList().get(0);

        return em.merge(estado);
    }

    @Override
    public HorariosDoctor addHorario(HorariosDoctor horario) {
        return horariosDocRepository.save(horario);
    }

    @Override
    public HorariosDoctor updateHorario(HorariosDoctor horario) {
        return em.merge(horario);
    }

    @Override
    public List<Turno> searchByDate(Date date, Integer idDoctor) {
        return null;
    }

    @Override
    public void insertMultiplesTurnos(List<Turno> turnos) {
        for(Turno t : turnos){
            turnoRepository.save(t);
        }
    }
}

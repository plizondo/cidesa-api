package com.hayturno.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tomas on 13/03/16.
 */
@Entity
@Table(name = "estado")
@XmlRootElement(name="status")
public class Estado {

    @Id
    @Column(name = "idestado")
    private Integer id;

    @Column(name = "descripcion")
    private String descripcion;
    @Column(name="tipo_estado")
    private String estado;

    /****modifiers****/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Estado estado1 = (Estado) o;

        if (!id.equals(estado1.id)) return false;
        if (!descripcion.equals(estado1.descripcion)) return false;
        return estado.equals(estado1.estado);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + descripcion.hashCode();
        result = 31 * result + estado.hashCode();
        return result;
    }
}

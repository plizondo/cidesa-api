package com.hayturno.data.service.impl;

import com.hayturno.data.repository.EspecialidadRepository;
import com.hayturno.data.service.EspecialidadService;
import com.hayturno.entities.Especialidad;
import com.hayturno.entities.Localidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by tomas on 08/03/16.
 */
@Service
@EnableTransactionManagement
public class EspecialidadServiceImpl implements EspecialidadService{

    @Autowired
    EspecialidadRepository repository;

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Especialidad> findAllByLocalidad(Integer idLocalidad) {
        Query query = em.createQuery(" SELECT e FROM Persona p " +
                                "JOIN Doctor d " +
                                " JOIN Especialidad e" +
                                " JOIN Localidad l" +
                                " WHERE p.idLocalidad = :idlocalidad");

        query.setParameter("idlocalidad",idLocalidad);

        List<Especialidad> localidadList = query.getResultList();

        return localidadList;
    }

    @Override
    public Especialidad addEspecialidad(Especialidad e) {
        return repository.save(e);
    }

    @Override
    public void deleteEspecialidad(Integer id) {
        repository.delete(id);
    }
}

package com.hayturno.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tomas.lingotti on 3/8/2016.
 */
@Entity
@Table(name = "especialidad")
@XmlRootElement(name = "especialidades")
public class Especialidad {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idespecialidad" , nullable = false)
    private Integer id;

    @Column(name = "especialidad" , nullable = false)
    private String especialidad;

    /****modifiers****/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Especialidad that = (Especialidad) o;

        if (!id.equals(that.id)) return false;
        return especialidad != null ? especialidad.equals(that.especialidad) : that.especialidad == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (especialidad != null ? especialidad.hashCode() : 0);
        return result;
    }
}

package com.hayturno.data.repository;

import com.hayturno.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas on 13/03/16.
 */
public interface DoctorRepository extends JpaRepository<Doctor,Integer> {
}

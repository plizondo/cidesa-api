package com.hayturno.data.service;

import com.hayturno.entities.Doctor;
import com.hayturno.entities.Estado;
import com.hayturno.entities.HorariosDoctor;
import com.hayturno.entities.Turno;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Created by tomas on 13/03/16.
 */
public interface DoctorService  {

    /**
     *
     * @param  d - doctor to be added.
     * @return d - the entitiy added.
     */
    @Transactional
    Doctor addDoctor(Doctor d);

    /**
     *
     * @param id DoctorID to be deleted.
     */
    void deleteDoctor(Integer id);

    /**
     *
     * @param apellidoDoctor - search doctor by surname
     * @return a doctor's list with that surname.
     */
    List<Doctor> findDoctorByApellido(String apellidoDoctor);

    /**
     *
     * @return list with all the doctors availables
     */
    List<Doctor> findAll();

    /**
     *
     * @param doctor - the doctor to be updated
     * @return the doctor updated
     */
    Doctor updateDoctor(Doctor doctor);

    /**turnos**/


    /**
     *
     * @param t - turn to be added
     * @return that turn
     */
    @Transactional
    Turno addTurno(Turno t);

    /**
     *
     * @param idTurno - id of the turn to be deleted
     */
    void deleteTurno(Integer idTurno);

    /**
     *
     * @param date
     * @return the list of turns in that date
     */
    List<Turno> findTurnoByFecha(Date date);

    /**
     *
     * @param turno - to be updated
     * @return the updated turn
     */
    Turno updateTurno(Turno turno);

    /**
     *
     * @param idTurno to get the current status for that turn
     * @return the updated status.
     */
    Estado updateEstado(Integer idTurno);


    /**horarios**/

    /**
     *
     * @param horario - the new schedule
     * @return the schedule added.
     */
    HorariosDoctor addHorario(HorariosDoctor horario);

    /*List<HorariosDoctor> searchHorarioByDate(Date date);

    List<HorariosDoctor> searchHorarioByTime(Time time);*/

    /**
     *
     * @param horario - schedule to be updated
     * @return the updated schedule
     */
    HorariosDoctor updateHorario(HorariosDoctor horario);


    /**turnos**/

    /**
     * search turn in a specific date, with a specific doctor
     * @param date
     * @param idDoctor
     * @return
     */
    List<Turno> searchByDate(Date date, Integer idDoctor);

    /**
     *
     * @param turnos - multiples turns to be added
     */
    void insertMultiplesTurnos(List<Turno> turnos);


}

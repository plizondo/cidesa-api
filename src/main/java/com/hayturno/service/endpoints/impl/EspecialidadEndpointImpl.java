package com.hayturno.service.endpoints.impl;

import com.hayturno.data.service.EspecialidadService;
import com.hayturno.entities.Especialidad;
import com.hayturno.service.endpoints.EspecialidadEndpoint;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by tomas on 08/03/16.
 */
public class EspecialidadEndpointImpl implements EspecialidadEndpoint {

    @Autowired
    EspecialidadService service;

    @Override
    public List<Especialidad> findAllByLocalidad(Integer idLocalidad) {

        List<Especialidad> especialidadList = service.findAllByLocalidad(idLocalidad);
        return especialidadList;
    }

    @Override
    public Response addEspecialidad(Especialidad e) {
        Especialidad resource = service.addEspecialidad(e);
        return Response.status(Response.Status.CREATED).entity(resource).build();

    }
}

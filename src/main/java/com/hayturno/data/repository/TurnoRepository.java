package com.hayturno.data.repository;

import com.hayturno.entities.Turno;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas on 13/03/16.
 */
public interface TurnoRepository extends JpaRepository<Turno,Integer> {
}

package com.hayturno.data.service.impl;

import com.hayturno.data.repository.ProvinciaRepository;
import com.hayturno.data.service.ProvinciaService;
import com.hayturno.entities.Localidad;
import com.hayturno.entities.Provincia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by tomas on 08/03/16.
 */
@Service
@EnableTransactionManagement
public class ProvinciaServiceImpl implements ProvinciaService {

    @Autowired
    ProvinciaRepository provinciaRepository;

    @PersistenceContext
    EntityManager manager;

    @Override
    public List<Provincia> findAll() {
        return provinciaRepository.findAll();
    }


    @Override
    public void delete(int id) {
        provinciaRepository.delete(id);
        manager.flush();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Provincia addProvincia(Provincia provincia) {
        return provinciaRepository.save(provincia);
    }

    @Override
    public List<Localidad> getLocalidadesByProvincia(Integer idProvincia) {
        Query q = manager.createQuery("select l from Localidad l where l.idProvincia = :idprovincia");
        q.setParameter("idprovincia",idProvincia);
        List<Localidad> localidadList = (List<Localidad>) q.getResultList();
        return localidadList;
    }
}

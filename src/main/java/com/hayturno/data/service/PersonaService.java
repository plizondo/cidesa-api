package com.hayturno.data.service;

import com.hayturno.entities.Control;
import com.hayturno.entities.Persona;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

/**
 * Created by tomas.lingotti on 3/9/2016.
 */
public interface PersonaService {

    @Transactional(propagation = Propagation.REQUIRED)
    Persona addPersona(Persona p);

    List<Persona> serachPersona(Integer osid);

    //@valid @beanParam

    void deletePersona(Integer id);

    Persona updatePersona(Persona p);


    /**control**/

    List<Control> controles(Integer idPersona);
}

package com.hayturno.service.endpoints;

import com.hayturno.entities.Especialidad;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by tomas on 08/03/16.
 */

public interface EspecialidadEndpoint {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    List<Especialidad> findAllByLocalidad(@QueryParam("id") Integer idLocalidad);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/")
    Response addEspecialidad(Especialidad e);
}

package com.hayturno.data.repository;

import com.hayturno.entities.HorariosDoctor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas on 13/03/16.
 */
public interface HorariosDocRepository extends JpaRepository<HorariosDoctor,Integer> {
}

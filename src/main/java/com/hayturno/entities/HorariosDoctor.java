package com.hayturno.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Time;
import java.util.Date;

/**
 * Created by tomas on 13/03/16.
 */
@Entity
@Table(name = "horarios_doctor")
@XmlRootElement(name = "horarios")
public class HorariosDoctor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idhorarios_doctor")
    private Integer id;

    @Column(name = "fecha")
    private Date fecha;
    @Column(name = "hora_inicio")
    private Time horaIni;
    @Column(name = "hora_fin")
    private Time horaFin;
    @Column(name = "iddoctor")
    private Integer idDoctor;

    /****modifiers****/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Time getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(Time horaIni) {
        this.horaIni = horaIni;
    }

    public Time getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Time horaFin) {
        this.horaFin = horaFin;
    }

    public Integer getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Integer idDoctor) {
        this.idDoctor = idDoctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HorariosDoctor that = (HorariosDoctor) o;

        if (!id.equals(that.id)) return false;
        if (!fecha.equals(that.fecha)) return false;
        if (!horaIni.equals(that.horaIni)) return false;
        if (!horaFin.equals(that.horaFin)) return false;
        return idDoctor.equals(that.idDoctor);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + fecha.hashCode();
        result = 31 * result + horaIni.hashCode();
        result = 31 * result + horaFin.hashCode();
        result = 31 * result + idDoctor.hashCode();
        return result;
    }
}

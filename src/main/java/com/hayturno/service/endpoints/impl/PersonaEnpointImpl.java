package com.hayturno.service.endpoints.impl;

import com.hayturno.data.service.PersonaService;
import com.hayturno.entities.Persona;
import com.hayturno.service.endpoints.PersonaEndpoint;
import com.hayturno.service.endpoints.filters.PersonaFilter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.BeanParam;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by tomas on 09/03/16.
 */
public class PersonaEnpointImpl implements PersonaEndpoint {

    @Autowired
    PersonaService service;

    //TODO refactor this - do not work properly :(
    @Override
    public Response searchPersona(Integer osid) {
        List<Persona> personaList = service.serachPersona(osid);


        return Response.ok(personaList).build();
    }

    @Override
    public void deletePersona(Integer id) {
        service.deletePersona(id);
    }

    @Override
    public Response addPersona(Persona persona) {
        Persona personaResource = service.addPersona(persona);
        return Response.ok(Response.Status.CREATED).entity(personaResource).build();
    }
}

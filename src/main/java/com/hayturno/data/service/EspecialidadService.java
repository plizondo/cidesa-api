package com.hayturno.data.service;

import com.hayturno.entities.Especialidad;
import com.hayturno.entities.Localidad;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tomas.lingotti on 3/8/2016.
 */
public interface EspecialidadService {

    /**
     * find all the specialist by the city
     * @param idLocalidad
     * @return the list of specialist in that city.
     */
    List<Especialidad> findAllByLocalidad(Integer idLocalidad);

    /**
     * 
     * @param e
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    Especialidad addEspecialidad(Especialidad e);

    void deleteEspecialidad(Integer id);

}

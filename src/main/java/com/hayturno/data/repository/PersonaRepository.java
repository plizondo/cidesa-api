package com.hayturno.data.repository;

import com.hayturno.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.lingotti on 3/9/2016.
 */
public interface PersonaRepository extends JpaRepository<Persona,Integer> {
}

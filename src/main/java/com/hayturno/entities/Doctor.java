package com.hayturno.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tomas on 09/03/16.
 */
@Entity
@Table(name = "doctor")
@XmlRootElement(name = "doctores")
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "iddoctor", nullable = false)
    private Integer id;

    @Column(name = "idespecialidad" , nullable = false)
    private Integer idEspecialidad;

    @Column(name = "idpersona" , nullable = false)
    private Integer idPersona;

    /****modifiers****/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Doctor doctor = (Doctor) o;

        if (!id.equals(doctor.id)) return false;
        if (idEspecialidad != null ? !idEspecialidad.equals(doctor.idEspecialidad) : doctor.idEspecialidad != null)
            return false;
        return idPersona != null ? idPersona.equals(doctor.idPersona) : doctor.idPersona == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (idEspecialidad != null ? idEspecialidad.hashCode() : 0);
        result = 31 * result + (idPersona != null ? idPersona.hashCode() : 0);
        return result;
    }
}

package com.hayturno.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

/**
 * Created by tomas on 07/03/16.
 */
@Entity
@Table(name = "provincia")
@XmlRootElement(name = "provincias")
public class Provincia {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idprovincia")
    private Integer id;

    @Column(name = "provincia")
    private String provincia;

    public Integer getId() {
        return id;
    }

    /****modifiers****/

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Provincia provincia1 = (Provincia) o;

        if (!id.equals(provincia1.id)) return false;
        return Objects.equals(this.getId(),((Provincia) o).getId());

    }
}

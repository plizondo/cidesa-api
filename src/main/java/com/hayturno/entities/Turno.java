package com.hayturno.entities;



import scala.Int;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;
import java.sql.Time;

/**
 * Created by tomas on 13/03/16.
 */
@Entity
@Table(name = "turno")
@XmlRootElement(name = "turnos")
public class Turno {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idturno")
    private Integer id;
    @Column(name="fecha")
    private Date fecha;
    @Column(name="hora")
    private Time hora;
    @Column(name="turno_reasignado")
    private Boolean turnoReasignado;
    @Column(name = "orden_pagada")
    private String ordenPagada;
    @Column(name="iddoctor")
    private Integer idDoctor;
    @Column(name="idpersona")
    private Integer idPersona;
    @Column(name="idestado")
    private Integer idEstado;

    /****modifiers****/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public Boolean getTurnoReasignado() {
        return turnoReasignado;
    }

    public void setTurnoReasignado(Boolean turnoReasignado) {
        this.turnoReasignado = turnoReasignado;
    }

    public String getOrdenPagada() {
        return ordenPagada;
    }

    public void setOrdenPagada(String ordenPagada) {
        this.ordenPagada = ordenPagada;
    }

    public Integer getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Integer idDoctor) {
        this.idDoctor = idDoctor;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Turno turno = (Turno) o;

        if (!id.equals(turno.id)) return false;
        if (!fecha.equals(turno.fecha)) return false;
        if (!hora.equals(turno.hora)) return false;
        if (!idDoctor.equals(turno.idDoctor)) return false;
        if (!idPersona.equals(turno.idPersona)) return false;
        return idEstado.equals(turno.idEstado);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + fecha.hashCode();
        result = 31 * result + hora.hashCode();
        result = 31 * result + idDoctor.hashCode();
        result = 31 * result + idPersona.hashCode();
        result = 31 * result + idEstado.hashCode();
        return result;
    }
}

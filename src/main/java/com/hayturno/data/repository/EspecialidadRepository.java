package com.hayturno.data.repository;

import com.hayturno.entities.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.lingotti on 3/8/2016.
 */
public interface EspecialidadRepository extends JpaRepository<Especialidad,Integer> {
}

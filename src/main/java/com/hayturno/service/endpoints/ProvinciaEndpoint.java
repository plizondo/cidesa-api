package com.hayturno.service.endpoints;

import com.hayturno.entities.Especialidad;
import com.hayturno.entities.Localidad;
import com.hayturno.entities.Provincia;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by tomas on 08/03/16.
 */
public interface ProvinciaEndpoint {


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @CrossOriginResourceSharing(allowAllOrigins = true)
    @Path("/")
    List<Provincia> findAll();

    @GET
    @CrossOriginResourceSharing(allowAllOrigins = true)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/localidades")
    List<Localidad> localidadesPorProvincia(@PathParam("id") Integer idProvincia);
}
